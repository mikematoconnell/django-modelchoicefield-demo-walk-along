from django.shortcuts import redirect, render

from core.forms import DynamicUserCreateForm, UserCreateForm


# Create your views here.
def register(request):
    if request.method == "POST":
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("register")

    context = {"form": UserCreateForm}
    return render(request, 'core/register.html', context)


def dynamic_register(request):
    if request.method == "POST":
        form = DynamicUserCreateForm(request.POST)
        return redirect("register")
    
    context = {"form": DynamicUserCreateForm}
    return render(request, "core/register.html", context)