# Generated by Django 5.0 on 2024-01-18 00:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_remove_user_username'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='home_country',
        ),
    ]
