from django.urls import path

from . import views

urlpatterns = [
    path("", views.register, name='register'),
    path("register_dynamic/", views.dynamic_register, name='register_dynamic'),
]
