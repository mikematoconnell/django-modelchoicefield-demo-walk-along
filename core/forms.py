from django import forms
from django.contrib.auth.forms import UserCreationForm
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget
from dynamic_forms import DynamicField, DynamicFormMixin

from core.models import Car, User


class UserCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("email", "password1", "password2", "car_boolean", "car")
        widgets = {"home_country": CountrySelectWidget()}


class DynamicUserCreateForm(DynamicFormMixin, forms.Form):
    username = forms.CharField()
    password1 = forms.CharField()
    password2 = forms.CharField()

    car_boolean = forms.BooleanField()
    car = DynamicField(
        forms.ModelChoiceField,
        queryset=Car.objects.all(),
        initial=None
    )
    home_country = CountryField().formfield()
    